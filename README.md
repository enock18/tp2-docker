# Mise en place environnement wordpress
Prérequis: installer docker et docker-compose

1.Cliquer [ici](https://gitlab.com/enock18/tp2-docker) pour cloner le projet 
une fois que vous avez cloné le projet vous pouvez télécharger wordpress et le mettre dans le dossier `wp` à la racine
du projet assurez que `index.php` soit à l'interieur du dosier `wp`.

2.Lancer : `docker-compose up -d`

3.Allez à http://localhost:8080

4.Connection à la base de donnée vous trouvez le mot de pass et username sur fichier `docker-compose.yaml`

Problème:

Si vous n'arrivez pas à lancer l'application.
essayer `docker-compose up` si vous trouvez l'erreur ci-dessous dans votre terminal:
`listen tcp4 0.0.0.0:3306: bind: address already in use`
verifiez bien que vous n'avez pas mysql démarer sur votre pc, il se peut mysql utilise le port `3306`. Vous pouvez arrêtez mysql avec `sudo systemctl stop mysql` et `sudo systemctl status mysql` pour vérifiez qui l'est bien arrêté.